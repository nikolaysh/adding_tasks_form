<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('files', function (Blueprint $table) {
            $table->id();
            $table->string('uniqName');
            $table->string('originalName');
            $table->string('size')->nullable();
            $table->string('mime')->nullable();
            $table->unsignedBigInteger('task_id');
            $table->timestamps();

            $table->index('task_id', 'file_task_idx');
            $table->foreign('task_id', 'file_task_fk')->on('tasks')->references('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('files');
    }
}
