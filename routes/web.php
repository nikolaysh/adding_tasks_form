<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::group(['middleware' => ['auth']], function() {
    Route::group(['namespace' => 'Task'], function () {
        Route::get('/', 'IndexController');
        Route::get('task/{uniqName}/download', 'IndexController@download')->name('task.download');
    });

    Route::group(['namespace' => 'Admin', 'prefix' => 'admin', 'middleware' => ['admin']], function () {
        Route::get('/', 'IndexController')->name('admin.index');
        Route::group(['namespace' => 'Task', 'prefix' => 'task'], function () {
            Route::get('/', 'IndexController');
            Route::get('/create', 'CreateController')->name('admin.task.create');
            Route::post('/', 'StoreController')->name('admin.task.store');
        });
    });
});

Auth::routes();
