<?php

namespace App\Http\Requests\Admin\Task;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string',
            'description' => 'required|string',
            'files' => 'nullable|array',
            'files.*' => 'required|file|mimes:doc,docx,xls,xlsx,jpeg,png,zip',
            'executors' => 'required|array',
            'executors.*' => 'required|integer',
            'authorName' => 'required|string',
            'authorPhone' => array('nullable', 'string', 'regex:/^((\\+7)|8)[0-9]{10}/'),
            'authorEmail' => 'required|email'
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'Это поле необходимо для заполнения',
            'title.string' => 'Данные должны соответствовать строчному типу',
            'description.required' => 'Это поле необходимо для заполнения',
            'description.string' => 'Данные должны соответствовать строчному типу',
            'files.*.mimes' => 'Файлы должны соответствовать расширениям: doc,docx,xls,xlsx,jpeg,png,zip',
            'executors.required' => 'Это поле необходимо для заполнения',
            'authorName.required' => 'Это поле необходимо для заполнения',
            'authorName.string' => 'Имя автора должно быть строкой',
            'authorPhone.regex' => 'Телефон не соответствует шаблону',
            'authorEmail.require' => 'Это поле необходимо для заполнения',
            'authorEmail.email' => 'Введите адрес электронной почты',
        ];
    }
}
