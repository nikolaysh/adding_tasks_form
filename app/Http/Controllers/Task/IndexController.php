<?php

namespace App\Http\Controllers\Task;

use App\Http\Controllers\Controller;
use App\Models\File;
use App\Models\Task;
use Illuminate\Support\Facades\Storage;

class IndexController extends Controller
{
    public function __invoke()
    {
        $tasks = Task::paginate(5);

        return view('task.index', compact('tasks'));
    }

    public function download($uniqName)
    {
        $file = File::where('uniqName', $uniqName)->first();
        $originalName = $file->originalName;
        if(!$originalName) $originalName = $uniqName;
        return Storage::download('files/' . $uniqName, $originalName);
    }
}
