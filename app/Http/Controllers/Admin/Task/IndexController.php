<?php

namespace App\Http\Controllers\Admin\Task;

use function view;

class IndexController extends BaseController
{
    public function __invoke()
    {
        return abort(404);
    }
}
