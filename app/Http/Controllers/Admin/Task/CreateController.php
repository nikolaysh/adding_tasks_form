<?php

namespace App\Http\Controllers\Admin\Task;

use App\Models\User;
use function view;

class CreateController extends BaseController
{
    public function __invoke()
    {
        $executors = User::where('role', 1)->get();

        $thisUser = auth()->user();

        return view('admin.task.create', compact('executors', 'thisUser'));
    }
}
