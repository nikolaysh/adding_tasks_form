<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    use HasFactory;

    protected $table = 'tasks';
    protected $guarded = false;

    public function users() {
        return $this->belongsToMany(User::class, 'task_users', 'task_id', 'user_id');
    }

    public function author() {
        return $this->belongsTo(User::class, 'author_id', 'id');
    }

    public function files() {
        return $this->hasMany(File::class, 'task_id', 'id');
    }
}
