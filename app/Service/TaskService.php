<?php
namespace App\Service;

use App\Jobs\NotifyUserJob;
use App\Models\File;
use App\Models\Task;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class TaskService
{
    public function store($data) {
        try {
            DB::beginTransaction();

            $files = array();
            if(isset($data['files'])) {
                $files = $data['files'];
                unset($data['files']);
            }


            $executors = $data['executors'];
            unset($data['executors']);

            $thisUser = auth()->user();
            $thisUserEmail = $thisUser->email;

            if(trim($data['authorEmail']) != trim($thisUserEmail)) {
                throw new \Exception('Введенный Вами e-mail не совпадает с Вашим e-mail указанным при регистрации');
            }
            $author = array(
                'name' => $data['authorName'],
                'phone' => $data['authorPhone']
            );

            unset($data['authorName'], $data['authorPhone'], $data['authorEmail']);

            $data['author_id'] = $thisUser->id;

            $task = Task::firstOrCreate($data);

            foreach ($files as $file) {
                $addingFile = array();
                $addingFile['originalName'] = $file->getClientOriginalName();
                $addingFile['mime'] = $file->getClientMimeType();
                $addingFile['size'] = $file->getSize();
                $file = Storage::put('/files', $file);
                $addingFile['uniqName'] = str_replace('files/', '', $file);
                $addingFile['task_id'] = $task->id;

                File::create($addingFile);
            }

            $task->users()->attach($executors);

            if($author['name'] != $thisUser->name || $author['phone'] !=$thisUser->phone) {
                $thisUser->update($author);
            }

            NotifyUserJob::dispatch($task);

            DB::commit();
        } catch (Exception $exception) {
            DB::rollBack();
            dd($exception);
            abort(500);
        }
    }
}
