@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <table id="tasks-output" class="table table-striped">
                        <thead>
                        <tr>
                            <th>Название</th>
                            <th>Описание</th>
                            <th>Файлы</th>
                            <th>Исполнитель(ли)</th>
                            <th>ФИО автора</th>
                            <th>Телефон автора</th>
                            <th>Почта автора</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($tasks as $task)
                            <tr>
                                <td>{{ $task->title }}</td>
                                <td>{{ $task->description }}</td>
                                <td>
                                    @foreach($task->files as $file)
                                        <a href="{{ route('task.download', $file->uniqName) }}">{{ $file->originalName }}</a>
                                        </br>
                                    @endforeach
                                </td>
                                <td>
                                    @foreach($task->users()->get() as $user)
                                    {{ $user->name }}
                                    </br>
                                    @endforeach
                                </td>
                                <td>
                                    {{ $task->author->name }}
                                </td>
                                <td>
                                    {{ $task->author->phone }}
                                </td>
                                <td>
                                    {{ $task->author->email }}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                    <div class="mx-auto" style="margin-top:15px">
                        {{ $tasks->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
