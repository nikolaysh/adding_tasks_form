@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <form method="POST" action="{{ route('admin.task.store') }}" enctype="multipart/form-data">
                    @csrf

                    <div class="row mb-3">
                        <label for="title" class="col-md-4 col-form-label">Название</label>
                        <input id="title" type="text" class="form-control @error('title') is-invalid @enderror"
                               name="title" value="{{ old('title') }}" required>
                        @error('title')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="row mb-3">
                        <label for="description" class="col-md-4 col-form-label">Описание</label>
                        <textarea id="description" class="form-control
                             @error('description') is-invalid @enderror"
                                  name="description">{{ old('description') }}</textarea>

                        @error('description')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="row mb-3">
                        <label for="files" class="col-md-4 col-form-label">Прикрепить файлы</label>
                        <input id="files" type="file"
                               class="form-control @if ($errors->has('files.*')) is-invalid @endif"
                               name="files[]" multiple="multiple">

                        @if ($errors->has('files.*'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('files.*') }}</strong>
                            </span>
                        @endif
                    </div>


                    <div class="row mb-3">
                        <label for="executors" class="col-md-4 col-form-label">Выбрать Исполнителя(ей)</label>

                        <select id="executors" class="form-control @error('executors') is-invalid @enderror"
                                name="executors[]" multiple>
                            @foreach($executors as $executor)
                                <option value="{{ $executor->id }}"
                                    {{ $executor == (collect(old('executors')))->contains($executor) ? ' selected' : '' }}
                                >{{ $executor->name }}</option>
                            @endforeach
                        </select>

                        @error('executors')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>


                    <div class="row mb-3">
                        <label for="authorName" class="col-md-4 col-form-label">ФИО автора</label>
                        <input id="authorName" type="text"
                               class="form-control @error('authorName') is-invalid @enderror"
                               name="authorName" value="{{ old('authorName') ? old('authorName') : $thisUser->name }}" required>
                        @error('authorName')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="row mb-3">
                        <label for="authorPhone" class="col-md-4 col-form-label">Телефон автора</label>
                        <input id="authorPhone" type="tel" pattern="^(\+7|8)+[0-9]{10}"
                               class="form-control @error('authorPhone') is-invalid @enderror"
                               name="authorPhone" value="{{ old('authorPhone') ? old('authorPhone') : $thisUser->phone }}">
                        @error('authorPhone')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="row mb-3">
                        <label for="authorEmail"
                               class="col-md-4 col-form-label">{{ __('Адрес электронной почты автора') }}</label>

                        <input id="authorEmail" type="email"
                               class="form-control @error('authorEmail') is-invalid @enderror"
                               name="authorEmail" value="{{ old('authorEmail') ? old('authorEmail') : $thisUser->email }}" required autocomplete="email">

                        @error('authorEmail')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>

                    <div class="row mb-0">
                        <button type="submit" class="btn btn-primary">
                            {{ __('Register') }}
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
